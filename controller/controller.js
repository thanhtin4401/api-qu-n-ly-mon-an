export let foodController = {
    getInformationFromPage: () => {
        let foodName = document.getElementById('foodName').value;
        let Price = document.getElementById('Price').value * 1;
        let Description = document.getElementById('Description').value;
        let food = {
            name: foodName,
            price: Price,
            desciption: Description
        }
        return food;
    },
    showInformationToPage: (food) => {
        document.getElementById('foodName').value = food.name;
        document.getElementById('Price').value = food.price;
        document.getElementById('Description').value = food.description;
    },
    clearInputInPage: () => {
        document.getElementById('foodName').value = "";
        document.getElementById('Price').value = "";
        document.getElementById('Description').value = "";
    },
    noticeSuccess: (nameFunction) => {
        document.getElementById('success').classList.add("show");
        document.getElementById('success').classList.remove("hiden");
        document.querySelector('#success .content').innerHTML = `${nameFunction} success`;
        setTimeout(() => {
            document.getElementById('success').classList.remove("show");
            document.getElementById('success').classList.add("hiden");
        }, 3000)
    },
    noticeFail: (nameFunction) => {
        document.getElementById('failure').classList.add("show");
        document.querySelector('#fail .content').innerHTML = `${nameFunction} fail`;
        setTimeout(() => {
            document.getElementById('success').classList.remove("show");
            document.getElementById('success').classList.add("hiden");
        }, 3000)
    }
}