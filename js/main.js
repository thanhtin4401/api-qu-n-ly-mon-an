import { foodService } from "./Service/foodService.js";
import { snippingService } from "./Service/spinnerService.js"
import { foodController } from "../controller/controller.js"

let idFoodEdited = null;

let renderListFood = (list) => {
    let contentHTML = "";
    for (var i = 0; i < list.length; i++) {
        let food = list[i];
        let contentTr = `
        <tr>
            <td>${food.id}</td> 
            <td>${food.name}</td> 
            <td>${food.price}</td> 
            <td>${food.description}</td> 
            <td class="btn d-flex">   
            <button  onclick="editFood(${food.id})" class="btn btn-primary mr-2">Edit</button>
            <button class="btn btn-warning"onclick="deleteFoodList(${food.id})">Delete</button>
            </td>
        </tr>`
        contentHTML += contentTr;
    }
    document.getElementById('tbody_food').innerHTML = contentHTML;
}

let deleteFoodList = (idFood) => {
    snippingService.batLoading();
    foodService.deleteFood(idFood)
        .then((res) => {
            snippingService.tatLoading();
            foodController.noticeSuccess("delete food");
            renderFoodListService();
        })
        .catch((err) => {
            snippingService.tatLoading();
            foodController.noticeFail("delete food");
            console.log(err);
        })
}

window.deleteFoodList = deleteFoodList;

let renderFoodListService = () => {
    foodService.getListFood()
        .then((res) => {
            renderListFood(res.data);
        })
        .catch((err) => {
            console.log('err: ', err);

        })
}
renderFoodListService();

function addFood() {

    let food = foodController.getInformationFromPage();
    snippingService.batLoading();
    foodService.addFood(food)
        .then((res) => {
            snippingService.tatLoading();
            foodController.clearInputInPage();
            foodController.noticeSuccess("add food");
            renderFoodListService()
        })
        .catch((err) => {
            snippingService.tatLoading();
            foodController.noticeFail("add food");
            console.log(err);
        })
}

window.addFood = addFood;

function editFood(id) {
    snippingService.batLoading();
    idFoodEdited = id;
    foodService.getDetailInformationFood(id)
        .then((res) => {
            snippingService.tatLoading();
            document.getElementById("addFoodBtn").classList.add("disabled");
            document.getElementById("upDateFoodBtn").classList.remove("disabled");
            foodController.showInformationToPage(res.data)

        })
        .catch((err) => {
            snippingService.tatLoading();
            console.log(err);
        })

}

window.editFood = editFood;

function upDateInformation() {
    snippingService.batLoading();
    document.getElementById("addFoodBtn").classList.remove("disabled");
    document.getElementById("upDateFoodBtn").classList.add("disabled");



    let food = foodController.getInformationFromPage();
    let newFood = { ...food, id: idFoodEdited };
    foodService.upDateInforFood(newFood).
        then((res) => {
            snippingService.tatLoading();
            foodController.noticeSuccess("update food");

            renderFoodListService()
            foodController.clearInputInPage();
        })
        .catch((err) => {
            snippingService.tatLoading();
            foodController.noticeSuccess("update food");
            console.log(err);
        })


}

window.upDateInformation = upDateInformation;

let timeClickSuccess = () => {
    document.getElementById('success').classList.add("hiden");
    document.getElementById('success').classList.remove("show");

}

let timeClickFail = () => {
    document.getElementById('fail').classList.add("hiden");
    document.getElementById('fail').classList.remove("show");

}

window.timeClickSuccess = timeClickSuccess;
window.timeClickFail = timeClickFail;
