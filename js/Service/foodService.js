const BASE_URL = "https://62b0798d196a9e9870246138.mockapi.io/mon-an"

export let foodService = {
    getListFood: () => {
        return axios({
            url: BASE_URL,
            method: "GET"
        });
    },
    deleteFood: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE"
        });
    },
    addFood: (food) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            data: food
        })
    },
    getDetailInformationFood: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET"
        })
    },
    upDateInforFood: (food) => {
        return axios({
            url: `${BASE_URL}/${food.id}`,
            method: "PUT",
            data: food
        })
    }
}